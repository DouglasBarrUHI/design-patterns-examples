rmdir /s /q "builds/observer-example"
mkdir "builds/observer-example"

cmake . -G "Visual Studio 16 2019" -A x64 -B builds/observer-example
/usr/local/bin/cmake --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -DCMAKE_C_COMPILER:FILEPATH=/usr/bin/clang -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/clang++ -H/Users/doug/Dev/UHI/design-patterns-examples/observer/cpp/basic-example -B/Users/doug/Dev/UHI/design-patterns-examples/observer/cpp/basic-example/build -G "Unix Makefiles"

pause
