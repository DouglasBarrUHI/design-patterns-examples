#pragma once
#include <list>
#include <string>

#include "observer-interfaces.h"

/**
 * The TextEditor owns some important state and notifies observers when the state
 * changes.
 */

class TextEditor : public ObservableObject
{
public:
	
    /**
     * The subscription management methods.
     */
    void attach (Observer* observer) override;
    void detach (Observer* observer) override;
    void notify () override;

    void onEnterPressed (const std::string& message = "Empty");
    void updatedFromSavedState ();

private:
    std::list<Observer*> observers;
    std::string textEditorContents;
};